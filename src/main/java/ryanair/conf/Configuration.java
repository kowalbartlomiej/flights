package ryanair.conf;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;
import ryanair.flight.service.RouteService;
import ryanair.flight.service.ScheduleService;
import ryanair.flight.service.SearchFlightService;
import ryanair.flight.service.impl.DefaultScheduleService;
import ryanair.flight.service.impl.DefaultRouteService;
import ryanair.flight.service.impl.DefaultSearchFlightService;
import ryanair.flight.service.impl.RouteServiceInMemory;
import ryanair.flight.service.impl.ScheduleServiceInMemory;

@org.springframework.context.annotation.Configuration
public class Configuration {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }


    @Bean
    public ScheduleService scheduleService(RestTemplate restTemplate) {
        return new DefaultScheduleService(restTemplate);
    }

    @Bean
    public RouteService routeService(RestTemplate restTemplate) {
        return new DefaultRouteService(restTemplate);
    }

    @Bean
    public SearchFlightService searchFlightService(ScheduleService scheduleService, RouteService routeService){
        return new DefaultSearchFlightService(routeService, scheduleService);
    }

    /**
     *
     * only for test purpose
     */
    public SearchFlightService searchFlightServiceInMemory(){
        return new DefaultSearchFlightService(new RouteServiceInMemory(), new ScheduleServiceInMemory());
    }

}
