package ryanair.flight.rest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public abstract class RestSupport {
    public RestSupport(final RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    private final RestTemplate restTemplate;

    protected RestTemplate getRestTemplate(){
        return restTemplate;
    }

    protected HttpEntity<?> createHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        return new HttpEntity<>(headers);
    }
}
