package ryanair.flight.service;

import ryanair.flight.model.Schedule;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public interface ScheduleService {

    Schedule getSchedule(@NotBlank final String departure, @NotBlank final String arrival, @NotNull final int year, @NotNull final int month );

    void registerSchedule(Schedule schedule);
}
