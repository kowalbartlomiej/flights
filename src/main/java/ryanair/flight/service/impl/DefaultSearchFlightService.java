package ryanair.flight.service.impl;

import org.springframework.util.CollectionUtils;
import ryanair.flight.model.*;
import ryanair.flight.service.RouteService;
import ryanair.flight.service.ScheduleService;
import ryanair.flight.service.SearchFlightService;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DefaultSearchFlightService implements SearchFlightService {

    public DefaultSearchFlightService(final RouteService routeService, final ScheduleService scheduleService) {
        this.routeService = routeService;
        this.scheduleService = scheduleService;
    }

    private RouteService routeService;
    private ScheduleService scheduleService;


    public List<PossibleFlights> find(final SearchData searchData){
        List<PossibleFlights> flights = findDirectFlights(searchData);
        flights.addAll(findInterconnectedFlights(searchData));

        return flights;
    }

    public List<PossibleFlights> findDirectFlights(final SearchData searchData) {
        if (areDatesFromTheSameMonth(searchData.getDepartureDateTime(), searchData.getArrivalDateTime())) {
            throw new IllegalArgumentException("Not supported period");
        }

        List<PossibleFlights> possibleFlights = new ArrayList<>();

        Schedule schedule = getSchedule(searchData);

        if(Objects.nonNull(schedule)) {
            for (Day day : schedule.getDays()) {

                if (day.getDay() >= searchData.getDepartureDateTime().getDayOfMonth() && day.getDay() <= searchData.getArrivalDateTime().getDayOfMonth()) {

                    for (Flight flight : day.getFlights()) {
                        LocalDateTime fDepartureTime = LocalDateTime.of(searchData.getDepartureDateTime().getYear(), searchData.getDepartureDateTime().getMonth(), day.getDay(), Integer.valueOf(flight.getDepartureTime().substring(0, 2)), Integer.valueOf(flight.getDepartureTime().substring(3, 5)));
                        LocalDateTime fArriveTime = LocalDateTime.of(searchData.getDepartureDateTime().getYear(), searchData.getDepartureDateTime().getMonth(), day.getDay(), Integer.valueOf(flight.getArrivalTime().substring(0, 2)), Integer.valueOf(flight.getArrivalTime().substring(3, 5)));

                        if (fDepartureTime.compareTo(searchData.getDepartureDateTime()) >= 0 && fArriveTime.compareTo(searchData.getArrivalDateTime()) <= 0) {
                            addLeg(searchData.getDeparture(), searchData.getArrival(), searchData.getDepartureDateTime(), possibleFlights, day, flight);
                        }
                    }
                }
            }
        }

        return possibleFlights;
    }

    private Schedule getSchedule(final SearchData searchData) {
        Schedule schedule = null;
        try {
            schedule = scheduleService.getSchedule(searchData.getDeparture(), searchData.getArrival(), searchData.getDepartureDateTime().getYear(), searchData.getDepartureDateTime().getMonth().getValue());
        }catch(Exception e){
            e.printStackTrace();
        }
        return schedule;
    }


    public List<PossibleFlights> findInterconnectedFlights(final SearchData searchData){
        List<PossibleFlights> possibleFlights = new ArrayList<>();

        List<String>  commonAirports = calculateCommonAirport(searchData.getDeparture(), searchData.getArrival());

        for(String commonAirport : commonAirports){
            calculateFromArrivalToInterconnect(searchData, possibleFlights, commonAirport);
        }


        return possibleFlights;
    }

    private void calculateFromArrivalToInterconnect(final SearchData searchData , final List<PossibleFlights> possibleFlights, String commonAirport) {
        List<PossibleFlights> interconnectPossibleFlights = getInterconnectPosibleFlights(searchData, commonAirport);

        for(PossibleFlights interconnectFlights:interconnectPossibleFlights) {
            for(Leg interconnectLeg:interconnectFlights.getLegs()) {
                LocalDateTime departurePlus2H = interconnectLeg.getArrivalDateTime().plus(2, ChronoUnit.HOURS);
                SearchData fromCommonAirport = SearchData.builder().departure(commonAirport).arrival(searchData.getArrival()).departureDateTime(departurePlus2H).arrivalDateTime(searchData.getArrivalDateTime()).build();
                List<PossibleFlights> arrivalPossibleFlights = findDirectFlights(fromCommonAirport);
                if(!CollectionUtils.isEmpty(arrivalPossibleFlights)){

                    Leg legToInterconnect = Leg.builder()
                            .departureAirport(searchData.getDeparture())
                            .departureDateTime(interconnectLeg.getDepartureDateTime())
                            .arrivalAirport(interconnectLeg.getArrivalAirport())
                            .arrivalDateTime(interconnectLeg.getArrivalDateTime())
                            .build();

                    calculateFromInterconnectToArrival(possibleFlights, arrivalPossibleFlights, legToInterconnect);
                }
            }
        }
    }

    private void calculateFromInterconnectToArrival(List<PossibleFlights> possibleFlights, List<PossibleFlights> arrivalPossibleFlights, Leg legToInterconnect) {
        for(PossibleFlights arrivalPossibleFlight:arrivalPossibleFlights){
            for(Leg arrivalPossibleLeg:arrivalPossibleFlight.getLegs()){
                Leg legToArrival = Leg.builder()
                        .departureAirport(arrivalPossibleLeg.getDepartureAirport())
                        .departureDateTime(arrivalPossibleLeg.getDepartureDateTime())
                        .arrivalAirport(arrivalPossibleLeg.getArrivalAirport())
                        .arrivalDateTime(arrivalPossibleLeg.getArrivalDateTime())
                        .build();

                possibleFlights.add(PossibleFlights.builder().stops(1).legs(Arrays.asList(legToInterconnect, legToArrival)).build());

            }
        }
    }

    private List<PossibleFlights> getInterconnectPosibleFlights(final SearchData searchData, String commonAirport) {
        LocalDateTime arrivalMinus2H = searchData.getArrivalDateTime().minus(2, ChronoUnit.HOURS);

        SearchData toCommonAirport = SearchData.builder().departure(searchData.getDeparture()).arrival(commonAirport).departureDateTime(searchData.getDepartureDateTime()).arrivalDateTime(arrivalMinus2H).build();

        return findDirectFlights(toCommonAirport);
    }

    private List<String> calculateCommonAirport(String departure, String arrival) {
        List<String> from = new ArrayList<>();
        List<String> to = new ArrayList<>();
        List<Route> routes = routeService.getRoutes();

        for(Route route:routes){
            if(route.getAirportFrom().equals(departure)){
                from.add(route.getAirportTo());
            }

            if(route.getAirportTo().equals(arrival)){
                to.add(route.getAirportFrom());
            }
        }

        return findCommonElements(from, to);
    }

    private List<String> findCommonElements(List<String> from, List<String> to) {
        to.retainAll(from);
        return to;
    }

    private void addLeg(String departure, String arrival, LocalDateTime departureDateTime, List<PossibleFlights> possibleFlightsList, Day day, Flight flight) {
        Leg leg = buildLeg(departure, arrival, departureDateTime, day, flight);

        possibleFlightsList.add(PossibleFlights.builder().stops(0).legs(Arrays.asList(leg)).build());
    }

    private Leg buildLeg(String departure, String arrival, LocalDateTime departureDateTime, Day day, Flight flight) {
        LocalDateTime departureTime = LocalDateTime.of(departureDateTime.getYear(), departureDateTime.getMonth() ,day.getDay(), Integer.valueOf(flight.getDepartureTime().substring(0,2)), Integer.valueOf(flight.getDepartureTime().substring(3,5)));
        LocalDateTime arriveTime = LocalDateTime.of(departureDateTime.getYear(), departureDateTime.getMonth() ,day.getDay(), Integer.valueOf(flight.getArrivalTime().substring(0,2)), Integer.valueOf(flight.getArrivalTime().substring(3,5)));

        return Leg.builder().arrivalAirport(arrival).departureAirport(departure).departureDateTime(departureTime).arrivalDateTime(arriveTime).build();
    }

    private boolean areDatesFromTheSameMonth(LocalDateTime departureDateTime, LocalDateTime arrivalDateTime) {
        return departureDateTime.getYear() != arrivalDateTime.getYear()
                || departureDateTime.getMonth().getValue() != arrivalDateTime.getMonth().getValue();
    }

    public void registerSchedule(Schedule schedule){
        scheduleService.registerSchedule(schedule);
    }

    public void registerRoutes(List<Route> routes){
        routeService.registerRoutes(routes);
    }
}
