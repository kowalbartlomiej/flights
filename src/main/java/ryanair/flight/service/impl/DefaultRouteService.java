package ryanair.flight.service.impl;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ryanair.flight.model.Route;
import ryanair.flight.rest.RestSupport;
import ryanair.flight.service.RouteService;

import java.util.List;
import java.util.stream.Collectors;

public class DefaultRouteService extends RestSupport implements RouteService {

    private static final String URL = "https://services-api.ryanair.com/locate/3/routes";

    public DefaultRouteService(RestTemplate restTemplate) {
        super(restTemplate);
    }


    public List<Route> getRoutes() {

        List<Route> routes = callRestService();

        return routes.parallelStream()
                .filter(Route::isRyanairWithNoConnectingAirport)
                .collect(Collectors.toList());
    }

    @Override
    public void registerRoutes(List<Route> routes) {

    }

    private List<Route> callRestService() {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL);
        return getRestTemplate().exchange(
                builder.toUriString(),
                HttpMethod.GET,
                createHeaders(),
                new ParameterizedTypeReference<List<Route>>() {
                }).getBody();
    }
}
