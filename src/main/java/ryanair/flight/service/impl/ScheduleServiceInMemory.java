package ryanair.flight.service.impl;

import ryanair.flight.model.Schedule;
import ryanair.flight.service.ScheduleService;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ScheduleServiceInMemory implements ScheduleService {

    private Schedule schedule;

    // for tests only
    @Override
    public Schedule getSchedule(@NotBlank String departure, @NotBlank String arrival, @NotNull int year, @NotNull int month) {
        //We can implement here some logic
        return schedule;
    }

    public synchronized void registerSchedule(Schedule schedule){
        this.schedule = schedule;
    }
}
