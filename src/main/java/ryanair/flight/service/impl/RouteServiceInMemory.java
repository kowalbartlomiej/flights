package ryanair.flight.service.impl;

import ryanair.flight.model.Route;
import ryanair.flight.service.RouteService;

import java.util.ArrayList;
import java.util.List;

public class RouteServiceInMemory implements RouteService {
    private List<Route> routes = new ArrayList<>();

    @Override
    public List<Route> getRoutes() {
        return routes;
    }

    @Override
    public synchronized void registerRoutes(List<Route> routes) {
            this.routes = routes;
    }
}
