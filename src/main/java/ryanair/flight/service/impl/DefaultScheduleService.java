package ryanair.flight.service.impl;

import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import ryanair.flight.model.Schedule;
import ryanair.flight.rest.RestSupport;
import ryanair.flight.service.ScheduleService;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

public class DefaultScheduleService extends RestSupport implements ScheduleService {

    public DefaultScheduleService(RestTemplate restTemplate) {
        super(restTemplate);
    }

    private final String URL = "https://services-api.ryanair.com/timtbl/3/schedules/{departure}/{arrival}/years/{year}/months/{month}";

    public Schedule getSchedule(@NotBlank final String departure, @NotBlank final String arrival, @NotNull final int year, @NotNull final int month ) {

        Map<String, String> uriParams = new HashMap<String, String>();
        uriParams.put("departure", departure);
        uriParams.put("arrival", arrival);
        uriParams.put("year", String.valueOf(year));
        uriParams.put("month", String.valueOf(month));

        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL);

        System.out.println(builder.buildAndExpand(uriParams).toUri());

        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        HttpEntity<?> entity = new HttpEntity<>(headers);

        ResponseEntity<Schedule> response = getRestTemplate().exchange(
                builder.buildAndExpand(uriParams).toUriString(),
                HttpMethod.GET,
                entity,
                Schedule.class);


        return response.getBody();
    }

    @Override
    public void registerSchedule(Schedule schedule) {

    }
}
