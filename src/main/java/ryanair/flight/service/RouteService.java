package ryanair.flight.service;

import ryanair.flight.model.Route;

import java.util.List;

public interface RouteService {

    List<Route> getRoutes();
    void registerRoutes(List<Route> routes);
}
