package ryanair.flight.service;

import ryanair.flight.model.PossibleFlights;
import ryanair.flight.model.Route;
import ryanair.flight.model.Schedule;
import ryanair.flight.model.SearchData;

import java.util.List;

public interface SearchFlightService {
    List<PossibleFlights> find(SearchData searchData);

    List<PossibleFlights> findDirectFlights(final SearchData searchData);
    List<PossibleFlights> findInterconnectedFlights(final SearchData searchData);

    void registerSchedule(Schedule schedule);
    void registerRoutes(List<Route> routes);

}
