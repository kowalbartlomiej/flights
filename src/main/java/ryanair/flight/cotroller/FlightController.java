package ryanair.flight.cotroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import ryanair.flight.model.PossibleFlights;
import ryanair.flight.model.SearchData;
import ryanair.flight.service.SearchFlightService;
import ryanair.flight.service.impl.DefaultSearchFlightService;

import java.time.LocalDateTime;
import java.util.List;

@RestController
public class FlightController {

    @Autowired
    private SearchFlightService searchFlightService;

    @RequestMapping(value = "/interconnections", method = RequestMethod.GET)
    public List<PossibleFlights> interconnections(@RequestParam final String departure, @RequestParam final String arrival, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime departureDateTime, @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) final LocalDateTime arrivalDateTime){

        final SearchData searchData = SearchData.builder().departure(departure).arrival(arrival).departureDateTime(departureDateTime).arrivalDateTime(arrivalDateTime).build();
        List<PossibleFlights> flights= searchFlightService.find(searchData);

        return flights;
    }
}
