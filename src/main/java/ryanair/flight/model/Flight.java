package ryanair.flight.model;

import lombok.*;

@Getter @Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Flight{
    private String carrierCode;
    private String number;
    private String departureTime;
    private String arrivalTime;
}
