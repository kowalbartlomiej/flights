package ryanair.flight.model;

import lombok.*;

import java.util.ArrayList;
import java.util.Objects;

@Setter @Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Route {

    private static final String RYANAIR = "RYANAIR";

    private String airportFrom;
    private String airportTo;
    private String connectingAirport = null;
    private boolean newRoute;
    private boolean seasonalRoute;
    private String operator;
    private String group;
    ArrayList<Object> tags = new ArrayList<Object>();
    private String carrierCode;


    public boolean isRyanairWithNoConnectingAirport() {
        return Objects.isNull(this.getConnectingAirport()) && RYANAIR.equals(this.getOperator());
    }

}
