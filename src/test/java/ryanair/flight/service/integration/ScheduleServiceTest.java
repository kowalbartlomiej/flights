package ryanair.flight.service.integration;


import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ryanair.flight.model.Schedule;
import ryanair.flight.service.impl.DefaultScheduleService;

import java.time.LocalDateTime;

import static org.junit.Assert.assertNotNull;

public class ScheduleServiceTest extends BaseIntegraionTest {

    @Autowired
    private DefaultScheduleService scheduleService;

    @Test
    public void successfulRequest(){
        //given
        final String departure = "DUB";
        final String arrival = "WRO";

        LocalDateTime current = LocalDateTime.now();
        final int year = current.getYear();
        final int month = current.getMonth().getValue();

        //when
        Schedule schedule = scheduleService.getSchedule( departure, arrival, year, month);

        //then
        assertNotNull(schedule);
    }
}