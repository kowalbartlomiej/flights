package ryanair.flight.service.integration;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import ryanair.flight.model.Route;
import ryanair.flight.service.impl.DefaultRouteService;

import java.util.List;

import static org.junit.Assert.assertFalse;

public class RouteServiceIntegrationTest extends BaseIntegraionTest {

    @Autowired
    private DefaultRouteService routeService;

    @Test
    public void successfulRequest(){
        //when
        List<Route> routes = routeService.getRoutes();

        //then
        assertFalse(CollectionUtils.isEmpty(routes));

    }

}