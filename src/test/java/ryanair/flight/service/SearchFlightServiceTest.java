package ryanair.flight.service;

import org.junit.Test;
import ryanair.conf.Configuration;
import ryanair.flight.model.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class SearchFlightServiceTest {

    Configuration conf = new Configuration();
    SearchFlightService searchFlightService = conf.searchFlightServiceInMemory();



    @Test
    public void findFlightsWhereArrivalDayEqualsToDepartureDay(){
        //given
        final String departure = "DUB";
        final String arrival = "WRO";
        LocalDateTime departureTime = LocalDateTime.of(2019, 06, 2, 8,30);
        LocalDateTime arrivalTime = LocalDateTime.of(2019, 06, 2, 10,30);

        Schedule schedule = createSchedule();
        searchFlightService.registerSchedule(schedule);
        searchFlightService.registerRoutes(Collections.EMPTY_LIST);

        SearchData searchData = SearchData.builder().departure(departure).arrival(arrival).departureDateTime(departureTime).arrivalDateTime(arrivalTime).build();

        //when
        List<PossibleFlights> bleBles = searchFlightService.findDirectFlights(searchData);

        //then
        assertNotNull(bleBles);
        assertEquals(1, bleBles.size());

    }


    @Test
    public void findFlightsWhereArrivalDayIsGraterThenDepartureDay(){
        //given
        final String departure = "DUB";
        final String arrival = "WRO";
        LocalDateTime departureTime = LocalDateTime.of(2019, 06, 2, 8,30);
        LocalDateTime arrivalTime = LocalDateTime.of(2019, 06, 3, 10,30);

        searchFlightService.registerSchedule(createSchedule());
        searchFlightService.registerRoutes(Collections.EMPTY_LIST);

        SearchData searchData = SearchData.builder().departure(departure).arrival(arrival).departureDateTime(departureTime).arrivalDateTime(arrivalTime).build();

        //when
        List<PossibleFlights> flights = searchFlightService.findDirectFlights(searchData);

        //then
        assertNotNull(flights);
        assertEquals(5, flights.size());
    }


    @Test
    public void findInterconnectedFlights(){
        //given
        final String departure = "AAR";
        final String arrival = "STN";
        LocalDateTime departureTime = LocalDateTime.of(2019, 06, 2, 8,30);
        LocalDateTime arrivalTime = LocalDateTime.of(2019, 06, 3, 10,30);

        searchFlightService.registerSchedule(createSchedule());
        searchFlightService.registerRoutes(createRoutes());

        SearchData searchData = SearchData.builder().departure(departure).arrival(arrival).departureDateTime(departureTime).arrivalDateTime(arrivalTime).build();


        //when
        List<PossibleFlights> flights = searchFlightService.findInterconnectedFlights(searchData);

        //then
        assertNotNull(flights);
        assertEquals(7, flights.size());
    }



    private Schedule createSchedule() {
        Flight flight = Flight.builder().departureTime("08:30").arrivalTime("10:30").build();
        Flight flight2 = Flight.builder().departureTime("08:29").arrivalTime("10:30").build();
        Flight flight3 = Flight.builder().departureTime("08:30").arrivalTime("10:31").build();
        Flight flight4 = Flight.builder().departureTime("12:30").arrivalTime("13:31").build();

        List<Flight> flights = Arrays.asList(flight, flight2, flight3, flight4);
        Day day = Day.builder().day(1).flights(flights).build();
        Day day2 = Day.builder().day(2).flights(flights).build();
        Day day3 = Day.builder().day(3).flights(flights).build();
        Day day4 = Day.builder().day(4).flights(flights).build();

        List<Day> days = Arrays.asList(day, day2, day3, day4);

        return Schedule.builder().month(6).days(days).build();
    }

    private List<Route> createRoutes(){

        List<Route> routes = new ArrayList<>();

        routes.add(Route.builder().airportFrom("AAR").airportTo("GDN").build());
        routes.add(Route.builder().airportFrom("GDN").airportTo("STN").build());

        return routes;
    }
}